#!/usr/bin/env python3

# importing ros stuff
from launch import LaunchDescription
from launch_ros.actions import Node

# Own stuff
from idl_botsy_pkg.softwareConfigutarion import simulation

# Importing os
import os

# Importing pathlib
from pathlib import Path




def generate_launch_description():
    ld = LaunchDescription()

    
    # Transform publisher node
    tf_node = Node(     package = 'idl_transform_pkg',
                        node_executable = 'ros_node_tf_odom',
                        output= 'screen',
                        emulate_tty = True,
                        arguments = [('__log_level:=info')])

    # Rviz2
    # Getting path to cwd
    home = str(Path.home())
    rvizConfPathString = home + '/colcon_botsy/src/idl_botsy_pkg/rviz/botsy_PointCloud_config.rviz'
    rv_node = Node(     package = 'rviz2',
                        node_executable = 'rviz2',
                        output= 'screen',
                        emulate_tty = True,
                        arguments = [('__log_level:=info'),'-d', rvizConfPathString])
    
    # PC msgs needs restamping to fit with rviz
    if simulation:
        # pc restamper publisher node
        pc_node = Node(     package = 'idl_transform_pkg',
                            node_executable = 'ros_node_pc2_restamper',
                            output= 'screen',
                            emulate_tty = True,
                            arguments = [('__log_level:=info')])
    else:
        pc_node = Node(     package = 'idl_transform_pkg',
                            node_executable = 'ros_node_zedm_restamper',
                            output= 'screen',
                            emulate_tty = True,
                            arguments = [('__log_level:=info')])

    


    # Adding nodes to launch description
    ld.add_action(tf_node)
    ld.add_action(rv_node)
    ld.add_action(pc_node)



    return ld