#!/usr/bin/env python3

# importing ros stuff
from launch import LaunchDescription
from launch_ros.actions import Node

# Own stuff
from idl_botsy_pkg.softwareConfigutarion import simulation



def generate_launch_description():
    ld = LaunchDescription()

    # Kalman filter node
    kf_node = Node(     package = 'idl_orientation_pkg',
                        node_executable = 'ros_node_kf',
                        output= 'screen',
                        emulate_tty = True,
                        arguments = [('__log_level:=info')])

    # Particle filter node
    pf_node = Node(     package = 'idl_pf_pkg',
                        node_executable = 'ros_node_pf',
                        output= 'screen',
                        emulate_tty = True,
                        arguments = [('__log_level:=info')])


    # Adding nodes to launch description
    ld.add_action(kf_node)
    ld.add_action(pf_node)


    return ld