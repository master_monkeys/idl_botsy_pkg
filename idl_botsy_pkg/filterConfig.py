


'''
File is only intended to hold drone configration data
'''

import numpy as np

from idl_botsy_pkg.droneConfiguration import DroneGeometry


### Global variables ###
kalmanFilterConfigurationJitCompile = True
kalmanFilterConfigurationSplit = False



## Structs to hold data
class Vec3(object):

    def __init__(self, x = 0.0,y = 0.0 ,z = 0.0):
        self.x = x
        self.y = y
        self.z = z

    def asNpArray(self, shape = (3,1)):
        return np.array([self.x,self.y,self.z], dtype = np.float32).reshape(shape)


class FilterInitialStates(object):

    def __init__(self):

        ### Initial states
        # Position Related
        self.pos        = Vec3(x=0.0, y=0.0, z=-0.05)
        self.tHeta      = Vec3(x=0.0, y=0.0, z=1.57)

        # Velocity Related
        self.linVel     = Vec3(x=0.0, y=0.0, z=0.0)
        self.angVel     = Vec3(x=0.0, y=0.0, z=0.0)

        # Acceleration Related
        self.linAcc     = Vec3(x=0.0, y=0.0, z=0.0)

        # Sensor Biases
        self.accBias    = Vec3(x=0.0, y=0.0, z=0.0)
        self.omgBias    = Vec3(x=0.0, y=0.0, z=0.0)

        ### Uncertainties
        # Position Related
        self.posCov     = Vec3(x=1.0, y=1.0, z=0.1)
        self.tHetaCov   = Vec3(x=0.01, y=0.01, z=0.25)

        # Velocity Related
        self.linVelCov  = Vec3(x=1.0, y=1.0, z=1.0)
        self.angVelCov  = Vec3(x=0.01, y=0.01, z=0.01)

        # Acceleration Related
        self.linAccCov  = Vec3(x=0.01, y=0.01, z=0.01)

        # Sensor Biases
        self.accBiasCov = Vec3(x=0.00001, y=0.00001, z=0.00001)
        self.omgBiasCov = Vec3(x=0.00001, y=0.00001, z=0.00001)
        

class FilterConfiguration(object):

    def __init__(self):

        # Configures filter to use gazebo ground truth data, only available in simulation mode
        self.gazeboGT = False
        # Delta imu msg means that the IMU data is integrated between predicts, if False the last received imu data is used for predict
        self.deltaImuCum = True
        # Using second order predict, can be beneficial if predict rate is low
        self.secondOrderPredict = True
        # Fixed rate predict, dose predicts at a timer, insted of in IMU callback function
        self.fixedRatePredict = True


### EKF imu settings ###

# Gazebo IMU tuning
class ImuGazeboMain(object):
    
    def __init__(self):
        # System state predict uncertainty matrix
        

        self.qPosition   = 0.5
        self.qAngles     = 0.5
        self.qLinVel     = 0.5
        self.qAngVel     = 0.5
        self.qLinAcc     = 0.01
        self.qAngAcc     = 0.1
        self.qBiasAcc    = 0.0001
        self.qBiasGyro   = 0.00003
        self.qGravity    = 0.00001

        # Measurement uncertainties covariance
        self.rYaw   = 0.1
        self.rPos   = 0.1
        self.rAcc   = 0.01
        self.rGyro  = 0.01
        self.rLevel = 250.0

        # Acc related
        self.levelingWindow = 0.05

        # Sensor localization
        droneGeom = DroneGeometry()
        self.rotMat_bs = droneGeom.rotMat_bu
        self.pos_b_bs = droneGeom. pos_b_bu

        # Position and yaw threshold
        self.posThreshold = 5.0
        self.yawThreshold = 1.57

        # Time stuff
        self.timeMaxDelayPose = 10.0

# Gazebo PX4 imu tuning
class ImuPX4SimMain(object):
    
    def __init__(self):
        # System state predict uncertainty matrix
        

        self.qPosition   = 0.5
        self.qAngles     = 0.5
        self.qLinVel     = 0.5
        self.qAngVel     = 0.5
        self.qLinAcc     = 0.01
        self.qAngAcc     = 0.1
        self.qBiasAcc    = 0.0001
        self.qBiasGyro   = 0.00003
        self.qGravity    = 0.00001

        # Measurement uncertainties covariance
        self.rYaw   = 0.1
        self.rPos   = 0.1
        self.rAcc   = 0.01
        self.rGyro  = 0.01
        self.rLevel = 250.0

        # Acc related
        self.levelingWindow = 0.05

        # Sensor localization
        droneGeom = DroneGeometry()
        self.rotMat_bs = droneGeom.rotMat_bu
        self.pos_b_bs = droneGeom. pos_b_bu

        # Position and yaw threshold
        self.posThreshold = 5.0
        self.yawThreshold = 1.57

        # Time stuff
        self.timeMaxDelayPose = 10.0

# Physical PX4 IMU tuning
class ImuPX4RealMain(object):
    
    def __init__(self):
        # System state predict uncertainty matrix
        

        self.qPosition   = 0.5
        self.qAngles     = 0.5
        self.qLinVel     = 0.5
        self.qAngVel     = 0.5
        self.qLinAcc     = 0.01
        self.qAngAcc     = 0.1
        self.qBiasAcc    = 0.0005
        self.qBiasGyro   = 0.0001
        self.qGravity    = 0.00001

        # Measurement uncertainties covariance
        self.rYaw   = 0.1
        self.rPos   = 0.1
        self.rAcc   = 0.01
        self.rGyro  = 0.01
        self.rLevel = 50.0

        # Acc related
        self.levelingWindow = 0.10

        # Sensor localization
        droneGeom = DroneGeometry()
        self.rotMat_bs = droneGeom.rotMat_bu
        self.pos_b_bs = droneGeom. pos_b_bu

        # Position and yaw threshold
        self.posThreshold = 5.0
        self.yawThreshold = 1.57

        # Time stuff
        self.timeMaxDelayPose = 10.0

# Ekf config
class EkfRates(object):

    def __init__(self):
        self.ekfInsPredictHz = 75.0
        self.ekfServiceHz = 2.0
        self.ekfVelBodyPubHz = 2.0
        self.ekfVelLevelPubHz = 15.0
        self.ekfPosNedPubHz = 10.0
        self.ekfOdomNedPubHz = 10.0
        self.ekfSensorBiasPubHz = 10.0


### PF setup ###
class ParticleFilterSetup(object):

    def __init__(self):

        ''' PF operation specific params '''
        # Number of particles to use in PF
        self.numParticles = 1500

        # Threshold for resampling, resampling occurs if effective sample size is lower than threshold
        self.resamplingThreshold = self.numParticles

        # Integration methods, propagation
        self.secondOrderIntegration = False
        self.deltaPositionIntegration = False

        # Parameters for PF covariance
        # Watchdog gain for decay of velocity when no message received
        self.wd_K               = 0.05

        # Maximum value for added propogation std.deviation when no message is received
        self.maxVelStdCtr       = 3.0

        # Const covariance added to propogation
        self.constVar           = Vec3(x=1.0, y=1.0, z=1.0) 
        self.constVarPsi        = 0.5

        # Method for pointcloud update step, use square sum method over product
        self.pcUpdateSqSum = False


        ''' Particle pointcloud publisher '''
        # Bools to signify whether or not to publish
        self.pubPFParticlePC    = True

        # Number of particles to publish
        self.pfPCSize           = 50

        # Rate of publish [Hz]
        self.pfPCPublisherRate  = 5


        ''' Sensor parameters '''
        # Helpers
        z_hit = 0.8
        z_rand = 0.2
        z_max = 1.0

        # The sum: z_hit + z_rand/z_max is defined to be equal 1.0
        z_sum = z_hit + z_rand/z_max

        # Setting sensor parameter values
        self.pf_z_hit           = z_hit / z_sum
        self.pf_z_rand          = z_rand / z_sum
        self.pf_z_max           = z_max / z_sum

        # Max range of sensor
        self.pf_sensMaxRange    = 15.0

        # Number of points to use from pointcloud
        self.nPts_PC            = 90


        ''' Parameters for configuring pointcloud downsampling '''
        # Use random points when downsampling
        # Selects nPts randomly, and then checks for duplicates and max_range measurements
        # deletes dupes and max_range measurements from pointcloud being passed on
        self.pcdsRandPoints = True

        # Select particles in a loop, checking each point for validity (range, dupe) before adding to an array
        # IF BOTH pcdsLoopSelect AND pcdsRandPoints IS SET TRUE, DEFAULTS TO LOOP CHECK MODE
        self.pcdsLoopSelect = False
        self.pcdsLoopSelMaxLoops = 2*self.nPts_PC


        ''' Histogram smoothing '''
        # Resolution
        self.histRes            = 0.01

        # std deviation
        self.histGaussStdDev    = 0.1


class MapConfig(object):
    
    def __init__(self):
        '''
            Maps that are currently available:
                UiABasementU8     -   Basement hallways at UiA, uint8 prob
                IndustrialU8      -   Industrial map found online, uint8 prob
                SimpleU8          -   Simple map with shapes made in solidworks, uint8 prob
        '''

        self.mapName = 'UiABasementU8'
