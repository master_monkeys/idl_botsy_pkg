
'''
File is only intended to hold drone configration data

Drone geometry class is intended for static transforms and dynamic transforms between frames
'''

import numpy as np
from idl_botsy_pkg.softwareConfigutarion import simulation


'''
https://www.fossen.biz/wiley/ed2/Ch2.pdf
'''

class Rot(object):

    def __init__(self):
        pass

    def rotX(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ 1, 0, 0],
                            [ 0, c,-s],
                            [ 0, s, c]])

    def rotY(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ c, 0, s],
                            [ 0, 1, 0],
                            [-s, 0, c]])

    def rotZ(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ c,-s, 0],
                            [ s, c, 0],
                            [ 0, 0, 1]])

    def rotXYZ(self, arg):

        '''
        Rotates Rx*Ry*Rz (xyz)

        input   arg is numpy array with shape (3,1)
        output  is a np mat with shape (3,3)
        '''
        
        # Parses data
        phi     = arg[0, 0]
        theta   = arg[1, 0]
        psi     = arg[2, 0]
        
        return self.rotX(phi) @ self.rotY(theta) @ self.rotZ(psi)

    def rotZYX(self, arg):

        '''
        Rotates Rz*Ry*Rx (zyx)

        input   arg is numpy array with shape (3,1)
        output  is a np mat with shape (3,3)
        '''
        
        # Parses data
        phi     = arg[0, 0]
        theta   = arg[1, 0]
        psi     = arg[2, 0]
        
        return self.rotZ(psi) @ self.rotY(theta) @ self.rotX(phi)


class DroneGeometry(object):

    def __init__(self):
        '''
        convention:
        trans_a_bc is a vector resolved in a describing some measure from b to c

        tHeta_bc is a vector of euler angle describing the relation from frame b to frame c

        rotMat_bc is a DCM matrix describing the relation ship from frame b to frame c... i.e c(tHeta_bc) gives a dcm from frame b to c

        rotFun_xy returns a rot mat

        
        Imu_main frame is denoted u
        Imu_aux frame is denoted w
        Imu_aux_virtual frame is denoted v

        Camera frame is denoted c

        Level frame is denoted l (this frame is a frame with it origin in body, and z in global ned z direction)

        Body (base_link) frame is denoted b
        '''

        # Creates instance of rotation class
        self.__rotation = Rot()


        ########################### World stuff ###########################

        # from global to map
        self.tHeta_nm = np.array([[0.0],[np.pi],[-np.pi/2]], dtype = np.float32)
        self.tHeta_mn = np.array([[0.0],[np.pi],[-np.pi/2]], dtype = np.float32)
        self.pos_n_nm = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)

        if simulation:
            ########################### Drone stuff ###########################
            ### offsets and misc

            # Camera tilt is the angle the camera is tilted downwards, i.e. if pointing towards the sky the tilt angle is negative
            # Also remember to change in .sdf file... not automated... :-( yet (left as an exrecice for the reader)
            self.camera_tilt = 15*np.pi/180


            ### Geometric vectors
            # body to imu main
            self.pos_b_bu = np.array([[0.0],[0.0],[-0.1]], dtype = np.float32)

            # body to imu aux
            self.pos_b_bv = np.array([[0.1],[0.0],[0.0]], dtype = np.float32)
            self.pos_b_bw = self.pos_b_bv

            # body to camera
            self.pos_b_bc = np.array([[0.1],[0.0],[0.0]], dtype = np.float32)
            
            ### Rotation "vector" in sequence zyx with elements [phi theta psi]
            # body to imu main
            self.tHeta_bu = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)

            # body to imu aux (virtual frame)
            self.tHeta_bv = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)
            

            # Body to camera (assumes zed mini imu is in same orientation as camera frame)
            self.tHeta_bc = np.array([[np.pi/2-self.camera_tilt],[0.0],[np.pi/2]], dtype = np.float32)
            self.tHeta_bw = self.tHeta_bc
        
        else:
            ########################### Drone stuff ###########################
            ### offsets and misc

            # Camera tilt is the angle the camera is tilted downwards, i.e. if pointing towards the sky the tilt angle is negative
            # Also remember to change in .sdf file... not automated... :-( yet
            self.camera_tilt = 20*np.pi/180


            ### Geometric vectors
            # body to imu main
            self.pos_b_bu = np.array([[-0.01],[0.0],[-0.08]], dtype = np.float32)

            # body to imu aux
            self.pos_b_bv = np.array([[0.1],[0.0],[0.0]], dtype = np.float32)
            self.pos_b_bw = self.pos_b_bv

            # body to camera
            self.pos_b_bc = np.array([[0.135],[-0.03],[0.0]], dtype = np.float32)
            
            ### Rotation "vector" in sequence zyx with elements [phi theta psi]
            # body to imu main
            self.tHeta_bu = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)

            # body to imu aux (virtual frame)
            self.tHeta_bv = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)
            

            # Body to camera (assumes zed mini imu is in same orientation as camera frame)
            self.tHeta_bc = np.array([[np.pi],[-self.camera_tilt],[0.0]], dtype = np.float32)
            self.tHeta_bw = self.tHeta_bc


        # Associated dcm
        self.rotMat_bu = self.__rotation.rotZYX(self.tHeta_bu)
        self.rotMat_bv = self.__rotation.rotZYX(self.tHeta_bv)
        self.rotMat_bc = self.__rotation.rotZYX(self.tHeta_bc)
        self.rotMat_bw = self.rotMat_bc
        
        self.rotMat_nm = self.__rotation.rotZYX(self.tHeta_nm)


    def rotFun_nb(self, tHeta_nb):
        '''
        Function to return dcm from ned to body given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        psi     = tHeta_nb[2, 0]


        return self.__rotation.rotX(-phi) @ self.__rotation.rotY(-theta) @ self.__rotation.rotZ(-psi)

    def rotFun_nl(self, tHeta_nb):
        '''
        Function to return dcm from ned to level given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        psi     = tHeta_nb[2, 0]


        return self.__rotation.rotZ(-psi)

    def rotFun_lb(self, tHeta_nb):
        '''
        Function to return dcm from level to body given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]


        return self.__rotation.rotX(-phi) @ self.__rotation.rotY(-theta)

    def rotFun_bn(self, tHeta_nb):
        '''
        Function to return dcm from body to ned given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        psi     = tHeta_nb[2, 0]


        return self.__rotation.rotZ(psi) @ self.__rotation.rotY(theta) @ self.__rotation.rotX(phi)

    def rotFun_bl(self, tHeta_nb):
        '''
        Function to return dcm from body to level given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        

        return self.__rotation.rotY(theta) @ self.__rotation.rotX(phi) 

    def rateTransform_nb(self, arg):
        '''
        Input is a np mat of shape (3,1)

        Output is a np mat of shape (3,3)

        Transforms body rates to global rates
        '''
        
        # Parses data
        phi     = arg[0][0]
        theta   = arg[1][0]

        # Pre calculating cos, sin and tan
        cx = np.cos(phi)
        cy = np.cos(theta)
        sx = np.sin(phi)
        ty = np.tan(theta)

        # Defines transform
        t = np.array([  [1, sx*ty,  cx*ty],
                        [0,    cx,    -sx],
                        [0, sx/cy,  cx/cy]])

        return t



def main():
    pass



if __name__ == '__main__':
    main()


