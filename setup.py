
import os
from glob import glob
from setuptools import setup

package_name = 'idl_botsy_pkg'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Include all launch files. This is the most important line here!
        (os.path.join('share', package_name), glob('launch/groundStation_launch.py')),
        (os.path.join('share', package_name), glob('launch/localization_launch.py')),
        (os.path.join('share', package_name), glob('launch/simulation_launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='void',
    maintainer_email='marius-egeland@hotmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
